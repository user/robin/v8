This file documents how to build the V8 libs in a form suitable for
use with MuPDF.

First, checkout Artifexes copy of V8 - currently:

  user@ghostscript.com:/home/robin/repos/v8.git/.git

or

  http://git.ghostscript.com/user/robin/v8.git/.git

Then change to the mupdf branch using git checkout. (Presumably you
have done this already to be reading this file).

To build on Windows:
~~~~~~~~~~~~~~~~~~~~

Open build/All.sln in VS2005 and build for both Release and Debug
configurations. VS2008 and VS2010 should work too after converting
the project files, but please do not publish such libraries as
they cannot be read by VS2005.

To build on Linux:
~~~~~~~~~~~~~~~~~~

make ia32 x64

That's it.

To build on MacOS X:
~~~~~~~~~~~~~~~~~~~~

export GYP_GENERATORS=make
export OUTDIR=out-mac
make ia32 x64

The GYP_GENERATORS line is required to keep us from producing xcode
projects (which don't work with xcode 4). The OUTDIR line is required
to separate the macos build products from the linux ones.

To build for Android:
~~~~~~~~~~~~~~~~~~~~~

We make use of another projects build scripts. On linux or MacOSX,
do:

  git checkout git://github.com/appcelerator/v8_titanium
  cd v8_titanium

Edit the .gitmodules file to change the url line:

[submodule "v8"]
        path = v8
        url = robin@ghostscript.com:/home/git/user/robin/v8.git/.git

Then:

  git submodule update --init

This will pull down our v8 source, and end with a complaint that the
specified revision is not present.

  cd v8
  git checkout mupdf
  cd ..

At this point you can build for the targets as follows:

  ./build_v8.sh -n /Library/android-ndk-r8d -j8 -l armeabi -p android-8 -s
  ./build_v8.sh -n /Library/android-ndk-r8d -j8 -l armeabi-v7a -p android-8 -s
  ./build_v8.sh -n /Library/android-ndk-r8d -j8 -l x86 -p android-9 -s
